#pliigo project creator
this is a little CLI tool, to quickly set up a web project

##installation
just install this module globally, to access it over the commandline
```
$ npm install pliigo-project-creator -g
```

##what it does
it is a little CLI tool, that offers you a guided walkthrough to select frontend (bower) packages and backend (npm) packages.
you just select the desired ones, and than the tool will create the needed files.
it also installs the bower and npm packages and creates an initial index.html file with the bower packages included.

it also generates the package.json and bower.json for you, as well an index.html with the components linked.

##usage

start just by typing "pliigo" into your shell

###example walkthrough
![example](https://bitbucket.org/repo/a6jed5/images/1496199737-Bildschirmfoto%202016-01-25%20um%2001.34.53.png)
###results into
![result](https://bitbucket.org/repo/a6jed5/images/2377913503-Bildschirmfoto%202016-01-25%20um%2001.39.06.png)


just type pliigo into your shell to use it
![pliigo](https://bitbucket.org/repo/a6jed5/images/4024886488-Bildschirmfoto%202016-01-24%20um%2005.58.55.png)

fill out the questions and select packages bower packages first
![bower](https://bitbucket.org/repo/a6jed5/images/3192183695-Bildschirmfoto%202016-01-24%20um%2005.59.19.png)

then select npm packages
![nodejs](https://bitbucket.org/repo/a6jed5/images/1983472062-Bildschirmfoto%202016-01-24%20um%2005.59.29.png)

finish up the remaining questions and then the project is lifted.

that's it.