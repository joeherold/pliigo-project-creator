/**
 * Created by johannespichler on 24.01.16.
 */

/**
 * import needed libraries
 */
var clc = require('cli-color');
var fs = require('fs');

/**
 *
 * @param text
 * @returns {string}
 */
var lineText = function (text) {
    var windowSize = process.stdout.getWindowSize();
    var sep = "";
    for (var i = 0; i < windowSize[0] - text.length; i++) {
        sep += " ";
    }

    return text + sep;
}
exports.lineText = lineText;

/**
 *
 * @returns {string}
 */
var emptyLine = function () {
    var windowSize = process.stdout.getWindowSize();
    var sep = "";
    for (var i = 0; i < windowSize[0]; i++) {
        sep += " ";
    }

    return sep;
}
exports.emptyLine = emptyLine;

/**
 *
 * @param text
 */
var initializeCliWindowWithText = function (text) {

    console.log("\u001b[2J\u001b[0;0H");
    console.log(clc.black.bgWhite(emptyLine()));
    console.log(clc.black.bgWhite(lineText(text)));
    console.log(clc.black.bgWhite(emptyLine()));

}
exports.initializeCliWindowWithText = initializeCliWindowWithText;


