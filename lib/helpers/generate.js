/**
 * Created by johannespichler on 24.01.16.
 */

var path = require("path");
var inquirer = require("inquirer");
var mkdirp = require("mkdirp");
var helper = require("./helper");
var settings = require("../../package.json");


// output first stuff to CLI
helper.initializeCliWindowWithText(" " + settings.name + " (v" + settings.version + ") ");


// define the current working directory
var absolutePath = process.cwd();

// extract base path (folder name)
var pathBase = path.basename(absolutePath);

var questions = [
    // in this directory
    {
        type: "list",
        name: "destination",
        message: "use this directory or create directory",
        choices: [
            {
                name: "this directory",
                default: true
            },
            {
                name: "create subdirectory by project name"
            }
        ]
    },
    {
        type: "input",
        name: "name",
        message: "name of your project",
        default: pathBase
    },
    {
        type: "checkbox",
        message: "select frontend frameworks (bower)",
        name: "fframeworks",
        choices: [
            {
                name: "modernizr-built",
                checked: true
            },
            {
                name: "jquery",
                checked: true
            },
            {
                name: "jquery-ui"
            },
            {
                name: "angularjs",
                checked: true
            },
            {
                name: "angular-ui-router"
            },
            {
                name: "angular-material"
            },
            {
                name: "angular-nvd3 "
            }
        ]
    },
    {
        type: "checkbox",
        message: "select server frameworks (NodeJS - npm)",

        name: "sframeworks",
        choices: [
            {
                name: "underscore",
                checked: true
            },
            {
                name: "async",
                checked: true
            },
            {
                name: "express"
            },
            {
                name: "passport"
            },
            {
                name: "grunt"
            },
            {
                name: "gulp"
            },
            {
                name: "webpack"
            },
            {
                name: "socket.io"
            },
        ]
    },
    {
        type: "input",
        name: "version",
        message: "project version",
        default: "1.0.0"
    },
    {
        type: "input",
        name: "project description",
        message: "description"
    },
    {
        type: "input",
        name: "project keywords",
        message: "keywords"
    },
    {
        type: "input",
        name: "author",
        message: "author",
        default: "Johannes Pichler - webpixels"
    },
    {
        type: "input",
        name: "license",
        message: "license",
        default: "MIT"
    },

];

inquirer.prompt(questions, function (answers) {
    var destination = absolutePath;
    if (answers.destination != "this directory") {
        destination = absolutePath + "/" + answers.name;
        mkdirp(destination, function (err) {

            // path was created unless there was error
            processAnswers(answers, destination);

        });

    } else {
        processAnswers(answers, destination);
    }


});


function processAnswers(answers, destination) {

    var package = require("../templates/package.json");
    var bower = require("../templates/bower.json");
    var bower_cli = require('bower');
    var _ = require("lodash");
    var async = require("async");
    var fs = require('fs');
    var clc = require('cli-color');
    var helper = require('./helper');

    // set package values
    package.name = answers.name;
    package.version = answers.version;
    package.description = answers.description;
    package.author = answers.author;
    package.license = answers.license;


    // set bower values
    bower.name = answers.name;
    bower.description = answers.description;
    bower.main = "./index.html";
    bower.license = answers.license;
    bower.homepage = "";
    bower.private = true;
    bower.dependencies = {};

    // run async tasks in waterfall
    async.waterfall([
        /**
         * writeConfigFiles
         * @param callback
         */
            function writeConfigFiles(callback) {

            // inform about creating config files
            console.log(clc.black.bgWhite(helper.lineText(" create default config files ")));


            // write package.json
            fs.writeFile(path.join(destination, 'package.json'), JSON.stringify(package, null, "  "), function (err) {
                if (err) {
                    callback(err, null);
                }
                // write bower.json
                fs.writeFile(path.join(destination, 'bower.json'), JSON.stringify(bower, null, "  "), function (err) {
                    if (err) {
                        callback(err, null);
                    }
                    // write readme.md
                    fs.writeFile(path.join(destination, 'readme.md'), "#" + answers.name + "\ndescribe your project here", function (err) {
                        if (err) {
                            callback(err, null);
                        }

                        // inform about finishing config files
                        console.log(clc.black.bgGreen(helper.lineText(" finished creating default config files ")));
                        // run callback
                        callback(null, 'writeConfigFiles');
                    });


                });

            });


        },
        /**
         * createBowerPackages
         * @param arg1
         * @param callback
         */
            function createBowerPackages(arg1, callback) {

            // inform about installing bower files
            console.log("\n"+clc.black.bgWhite(helper.lineText(" installing bower packages ")));
            bower_cli.commands
                .install(answers.fframeworks, {save: true}, {
                    cwd: destination
                })
                .on('end', function (installed) {
                    // inform about finishing installing bower packages
                    console.log(clc.black.bgGreen(helper.lineText(" finished installing bower packages ")));


                    // inform about creating index.html

                    console.log("\n"+clc.black.bgWhite(helper.lineText(" creating index.html and adding bower packages ")));


                    // create replacer for ###bower_components### replacement tag
                    var bower_components = "<!-- START: auto generated bower_components -->";

                    // fetch main paths for installed bower packages
                    bower_cli.commands.list({paths: true})
                        .on("end", function (data) {

                            // add jquery as first and remove from stack
                            if (data.jquery) {
                                bower_components += "\n    " + '<script src="' + data.jquery + '"></script>';
                            }
                            data.jquery = null;

                            // add jquery-ui as second and remove from stack
                            if (data['jquery-ui']) {
                                bower_components += "\n    " + '<script src="' + data['jquery-ui'] + '"></script>';
                            }
                            data['jquery-ui'] = null;

                            // add angularjs as third and remove from stack
                            if (data.angularjs) {
                                bower_components += "\n    " + '<script src="' + data.angularjs + '"></script>';
                            }
                            data.angularjs = null;

                            // add angular as angularjs fallback as fourth and remove from stack
                            if (data.angular) {
                                bower_components += "\n    " + '<script src="' + data.angular + '"></script>';
                            }
                            data.angular = null;


                            // iterate over the remaining stack
                            _.forEach(data, function (item) {

                                // ckeck if itam has more than one items (e.g. js & css)
                                if (_.isArray(item)) {
                                    _.forEach(item, function (subitem) {

                                        // add js
                                        if (path.extname(subitem) == ".js") {
                                            bower_components += "\n    " + '<script src="' + subitem + '"></script>';
                                        }
                                        // add css
                                        if (path.extname(subitem) == ".css") {

                                            bower_components += "\n    " + '<link rel="stylesheet" type="text/css"  href="' + subitem + '">'
                                        }

                                    })
                                }
                                // item is not an array. just check it against null value
                                else if (item !== null) {
                                    // add js
                                    if (path.extname(item) == ".js") {
                                        bower_components += "\n    " + '<script src="' + item + '"></script>';
                                    }
                                    // add css
                                    if (path.extname(item) == ".css") {

                                        bower_components += "\n    " + '<link rel="stylesheet" type="text/css"  href="' + item + '">'
                                    }
                                }
                            });

                            // end the replacer
                            bower_components += "\n    <!-- END: auto generated bower_components -->";


                            // reaad in the index.html
                            fs.readFile(path.join(__dirname, '../templates/index.html'), 'utf8', function (err, data) {
                                if (err) {
                                    callback(err, null);
                                }
                                ;

                                // create file write content with replaced content
                                var outHtml = data.replace("###bower_components###", bower_components);

                                // write new content to file
                                fs.writeFile(path.join(destination, 'index.html'), outHtml, function (err) {

                                    // inform about finishing index.html
                                    console.log(clc.black.bgGreen(helper.lineText(" finished creating index.html ")));


                                    callback(null, 'two');

                                });

                            });


                        })

                })
                .on('error', function (err) {
                    console.log("bower error:", err);
                    callback(err, 'two');
                })
                .on('log', function (log) {

                    // create a log output, to see whats going on inside the shell
                    if (log.level == "action" && log.id == "validate") {
                        console.log(
                            "bower: ",
                            clc.green(log.data.pkgMeta.name)
                            + " " + log.id
                        );
                    } else if (log.level == "action" && log.id == "extract") {
                        console.log(
                            "bower: ",
                            clc.yellow(log.data.endpoint.source)
                            + " " + log.id
                        );
                    } else if (log.level == "info") {
                        console.log(
                            "bower: ",
                            clc.yellow(log.data.endpoint.source) + " " + clc.red(log.data.endpoint.target)
                            + " " + log.id
                        );
                    } else {
                        console.log(
                            "bower: ",
                            clc.magenta(log.data.endpoint.source) + " " + clc.red(log.data.endpoint.target)
                            + " " + log.id
                        );
                    }

                })
        },


        /**
         * createNpmPackages
         * @param arg1
         * @param callback
         */
            function createNpmPackages(arg1, callback) {

            // inform about installing NPM packages
            console.log("\n"+clc.black.bgWhite(helper.lineText(" installing NPM packages (this may take a while)")));

            // use enpeem for mor easy usage
            var npm = require('enpeem');

            var installOptions = {
                dir: destination,
                dependencies: answers.sframeworks,
                save: true,
                'cache-min': 999999999, // from example of enpeem
                //loglevel: 'silent',
            }

            // call install command with gives options
            npm.install(installOptions, function (err) {
                if (err) {
                    return callback(err, null);
                }
                // inform about finished bower files
                console.log(clc.black.bgGreen(helper.lineText(" finished installing NPM packages ")));
                callback(null, 'one');
            });

        },

        /**
         * writePliigoConfig
         * @param arg1
         * @param callback
         */
            function writePliigoConfig(arg1, callback) {

            fs.writeFile(path.join(destination, 'pliigo.json'), JSON.stringify(answers, null, "  "), function (err) {
                if (err) {
                    callback(err, null);
                }
                console.log("\n"+clc.black.bgGreen(helper.lineText(" written pliigo.json ")));
                callback(null, 'done');
            });

        }
    ], function finalize(err, result) {
        if (err) {
            console.log(clc.black.bgRed(helper.lineText(" #### ERROR #### ")));
            console.log(err);
        }
        console.log("\n"+clc.black.bgGreen(helper.lineText(" #### FILE STRUCTURE CREATED #### ")));
        console.log(clc.black.bgGreen(helper.lineText(" #### ALL FINISHED #### ")));
    });
}




