/**
 * Created by johannespichler on 24.01.16.
 */


// import package.json
var settings = require("../package.json");


// initialize cli application
var program = require('commander');

program
    .version(settings.version)
    .command("generate", "generates a new project (is the default command)", {isDefault: true})
    .command("update", "updates an existing project")
    .parse(process.argv);